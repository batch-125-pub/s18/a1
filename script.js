let student = [];

function addStudent(name){
	student.push(name);
}


addStudent("Ryan");
addStudent("John");
addStudent("Lily");
addStudent("Cindy");
addStudent("Earl");

console.log(student);

function printStudents(){
student.forEach(
	function(element){
		console.log(element);
		}
	);

}

printStudents();

function countStudents(){
	return student.length;
}

console.log(countStudents());

function findStudent(name){
	if(student.includes(name)){
		return `${name} is found`;
	} else {
		return `${name} is not found`;
	}
}

console.log(findStudent("Lily"));
console.log(findStudent("Ryan"));
console.log(findStudent("Felix"));